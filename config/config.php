<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'excerpt-length' => 120,
];