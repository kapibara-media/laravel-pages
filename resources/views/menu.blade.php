@foreach($pages as $page)
<li class="lg:ml-2 {{ $classes }}">
    <a class="lg:py-2 lg:px-3 py-3 pl-3 pr-1 block rounded-lg hover:bg-gray-200 hover:text-gray-700 {{ Route::currentRouteName() === 'page.show' && \Str::endsWith(\request()->path(), $page->slug) ? 'bg-gray-700 text-white' : '' }}" href="/{{ $page->slug }}">
        {{ $page->label }}
    </a>
</li>
@endforeach