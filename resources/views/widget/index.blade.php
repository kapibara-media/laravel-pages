<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight flex flex-row justify-between items-center">
            {{ __('Widgets') }}
            <a href="{{ route('widget.create') }}"
                class="block py-2 px-3 my-1 rounded font-bold text-gray-600 hover:text-gray-100 bg-blue-300 hover:bg-blue-600">Voeg
                toe</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @foreach ($widgets as $widget)
                        <h4 class="text-lg">
                            <a href="{{ route('widget.edit', $widget) }}">{{ $widget->title }}
                                ({{ __($widget->type) }})
                            </a>
                        </h4>
                    @endforeach
                    @if ($widgets->isEmpty())
                        <p>Nog geen widgets gemaakt. <a href="{{ route('widget.create') }}"
                                class="text-blue-400 hover:text-blue-200">Voeg toe</a></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
