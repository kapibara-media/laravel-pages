<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bewerk deze widget') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form class="" action="{{ route('widget.update', $widget) }}" method="POST"  x-data="{ type: '{{ $widget->type }}' }">
                    @csrf
                    <div class="my-4">
                        <x-label :value="__('Widget type')" />
                        <div class="flex flex-row items-center">
                            {{ $widget->type }}
                        </div>
                    </div>
                    <div class="my-4">
                        <x-label for="title" :value="__('Widget titel')" />
                        <x-input id="title" class="w-full" type="text" name="title" :value="old('title', $widget->title)" required autofocus placeholder="Titel van de widget" />
                    </div>
                    <div class="my-4" x-show="type === 'text'">
                        <x-label for="label" :value="__('Tekst')" />
                        <x-editor class="w-full" name="body" id="body" :value="old('body', $widget->body)" />
                    </div>
                    <div class="my-4" x-show="['text', 'image'].includes(type)">
                        <x-label for="action_url" :value="__('Actie URL')" />
                        <x-input id="action_url" class="w-full" type="text" name="action_url" :value="old('action_url', $widget->action_url)" placeholder="URL van de actie-button" />
                    </div>
                    <div class="my-4" x-show="type === 'text'">
                        <x-label for="action_label" :value="__('Actie label')" />
                        <x-input id="action_label" class="w-full" type="text" name="action_label" :value="old('action_label', $widget->action_label)" placeholder="Label van de actie-button" />
                    </div>

                    <div class="my-4" x-show="type === 'image'">
                        <x-label for="img_url" :value="__('Beeld bron')" />
                        <x-input id="img_url" class="w-full" type="text" name="img_url" :value="old('img_url', $widget->img_url)" placeholder="URL van het beeld" />
                    </div>
                    <div class="my-4" x-show="type === 'image'">
                        <x-label for="img_alt" :value="__('Beeld omschrijving')" />
                        <x-input id="img_alt" class="w-full" type="text" name="img_alt" :value="old('img_alt', $widget->img_alt)" placeholder="Label van het beeld" />
                    </div>
                    <div class="my-4" x-show="type === 'image'">
                        <x-label for="img_caption" :value="__('Beeld onderschrift')" />
                        <x-input id="img_caption" class="w-full" type="text" name="img_caption" :value="old('img_caption', $widget->img_caption)" placeholder="Onderschrift bij het beeld" />
                    </div>

                    <div class="my-4" x-show="type === 'youtube'">
                        <x-label for="video_id" :value="__('Video ID')" />
                        <x-input id="video_id" class="w-full" type="text" name="video_id" :value="old('video_id', $widget->video_id)" placeholder="ID van de video" />
                    </div>
                    <div class="my-4" x-show="type === 'youtube'">
                        <x-label for="video_width" :value="__('Breedte')" />
                        <x-input id="video_width" class="w-full" type="number" name="video_width" :value="old('video_width', $widget->video_width)" placeholder="Breedte van de video in de website" />
                    </div>
                    <div class="my-4" x-show="type === 'youtube'">
                        <x-label for="video_height" :value="__('Hoogte')" />
                        <x-input id="video_height" class="w-full" type="number" name="video_height" :value="old('video_height', $widget->video_height)" placeholder="Hoogte van de video in de website" />
                    </div>

                    <div class="my-4 flex justify-between">
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('widget.index') }}">
                            {{ __('Annuleren') }}
                        </a>
                        <x-button>{{ __('Opslaan') }}</x-button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
