<aside {!! $attributes->merge(['class' => 'widget flex flex-col justify-between']) !!}>
    <span>
        <h3 class="widget-text-heading">{{ $widget->title }}</h3>
        @if ($widget->body)
            {!! $widget->body !!}
        @endif
    </span>
    @if ($widget->action_url)
        <x-action url="{{ $widget->action_url }}" class="widget-text-action">
            {{ $widget->action_label }}</x-action>
    @endif
</aside>
