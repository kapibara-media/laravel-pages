<input id="{{ $attributes['id'] }}" value="{{ $value }}" type="hidden" name="{{ $attributes['name'] }}" />
<trix-editor input="{{ $attributes['id'] }}" contenteditable="{{ $disabled ? 'false' : 'true' }}" {!! $attributes->merge(['class' => 'rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50']) !!}></trix-editor>
@if ($errors->has($attributes['name']))
    <div class="my-2 text-sm font-semibold text-red-400">{{ $errors->first($attributes['name']) }}</div>
@endif