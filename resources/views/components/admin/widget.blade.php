@props(['widgetLocation' => 'niet gelinkt', 'widget' => null, 'index' => 1, 'locations' => []])
{{-- <div class="flex-1">
    <input type="number" name="widgets[{{ $widget->id }}][order]"
        value="{{ $widget->pivot->order ?? 1 }}"
        class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" />
</div> --}}

<div class="widget-item py-3 text-lg flex justify-between" :class="{'bg-blue-100': dragging == {{ $index }}}"
    draggable="true" @dragstart.self="dragging={{ $index }}; drag_item=$event.target;"
    @dragend="dragging=null; dropping=null; saveOrder();" data-index="{{ $index }}"
    data-widget="{{ $widget->id }}">
    <div>
        <span class="mr-2 cursor-move">
            <svg class="inline-block h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                <path class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M4 6h16M4 12h16M4 18h16"></path>
            </svg>
        </span>
        <a href="{{ route('widget.edit', $widget) }}" title="bewerk">{{ $widget->title }}
            ({{ __($widget->type) }})</a>
    </div>
    <select name="widgets[{{ $widget->id }}][location]"
        class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
        <option value="">-- wordt niet getoond --</option>
        @foreach ($locations as $location)
            <option value="{{ $location }}" {{ $widgetLocation === $location ? 'selected' : '' }}>
                {{ $location }}</option>
        @endforeach
    </select>
    <input type="hidden" id="widget-order-{{ $index }}" class="widget-order-input"
        name="widgets[{{ $widget->id }}][order]" value="{{ $widget->pivot->order ?? 1 }}" />
</div>
<div @dragover.self.prevent="dropping={{ $index }}" class="w-full h-3 border-t border-gray-100"
    id="docking_{{ $index }}" :class="{'bg-green-100 h-6': dropping == {{ $index }}}"
    @drop="if (drag_item) { $event.target.after(drag_item); drag_item.after(document.getElementById('docking_'+drag_item.dataset.index)); drag_item=null; changed=true }">
</div>
