<aside {!! $attributes->merge(['class' => 'widget flex flex-col items-center']) !!}>
    <iframe width="{{ $widget->video_width ?? 560 }}" height="{{ $widget->video_height ?? 'auto' }}"
        src="https://www.youtube.com/embed/{{ $widget->video_id }}" title="YouTube video player" frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen></iframe>
</aside>
