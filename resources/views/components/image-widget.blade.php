<aside {!! $attributes->merge(['class' => 'widget flex flex-col items-center']) !!}>
    @if ($widget->action_url)
        <a href="{{ $widget->action_url }}" class="widget-image-anchor">
            <img class="widget-image" src=" {{ $widget->img_url }}" alt="{{ $widget->img_alt }}" />
        </a>
    @else
        <img class="widget-image" src=" {{ $widget->img_url }}" alt="{{ $widget->img_alt }}" />
    @endif
    @if ($widget->img_caption)
        <h4 class="widget-image-caption">{{ $widget->img_caption }}</h4>
    @endif
</aside>
