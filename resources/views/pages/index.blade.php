<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight flex flex-row justify-between items-center">
            {{ __("Pagina's") }}
            <a href="{{ route('page.create') }}"
                class="block py-2 px-3 my-1 rounded font-bold text-gray-600 hover:text-gray-100 bg-blue-300 hover:bg-blue-600">Voeg
                toe</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @foreach ($pages as $page)
                        <h4 class="py-3 text-lg flex justify-between">
                            <a href="{{ route('page.edit', $page) }}" title="bewerk">{{ $page->title }}</a>
                            <span>
                                <a href="{{ route('page.show', $page) }}" title="bekijk"><i
                                        class="fas fa-eye"></i></a>
                                @if ($page->published)
                                    <form method="POST" action="{{ route('page.unpublish', $page) }}"
                                        class="inline">
                                        @csrf
                                        <a href="publish"
                                            onclick="event.preventDefault(); this.closest('form').submit();"
                                            title="de-publiceer"><i class="fas fa-ban"></i></a>
                                    </form>
                                @else
                                    <form method="POST" action="{{ route('page.publish', $page) }}"
                                        class="inline">
                                        @csrf
                                        <a href="publish"
                                            onclick="event.preventDefault(); this.closest('form').submit();"
                                            title="publiceer"><i class="fas fa-paper-plane"></i></a>
                                    </form>
                                @endif
                            </span>
                        </h4>
                    @endforeach
                    @if ($pages->isEmpty())
                        <p>Nog geen pagina&rsquo;s gemaakt. <a href="{{ route('page.create') }}"
                                class="text-blue-400 hover:text-blue-200">Voeg toe</a></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
