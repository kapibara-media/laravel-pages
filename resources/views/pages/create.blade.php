<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Maak een nieuwe pagina') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form class="" action="{{ route('page.store') }}" method="POST">
                    @csrf
                    <div class="my-4">
                        <x-label for="title" :value="__('Pagina titel')" />
                        <x-input id="title" class="w-full" type="text" name="title" :value="old('title')" required autofocus placeholder="Titel van de pagina" />
                    </div>
                    <div class="my-4">
                        <x-label for="label" :value="__('Tekst')" />
                        <x-editor class="w-full" name="body" id="body" :value="old('body')" />
                    </div>
                    <div class="my-4">
                        <x-label for="label" :value="__('Navigatie label')" />
                        <x-input id="label" class="w-full" type="text" name="label" :value="old('label')" placeholder="Navigatielabel van de pagina" />
                    </div>
                    <div class="my-4 flex justify-between">
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('page.index') }}">
                            {{ __('Annuleren') }}
                        </a>
                        <x-button>{{ __('Opslaan') }}</x-button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
