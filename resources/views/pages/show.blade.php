<x-guest-layout>
    <x-slot name="title">
        {{ $page->label }} - {{ config('app.name', 'Laravel') }}
    </x-slot>
    @foreach ($widgets as $location => $widgets_on_that_location)
        <x-slot :name="__('widgets_:location', ['location'=>$location])">
            <div class="widgets {{ __('widgets-:location', ['location' => $location]) }} py-2">
                <div class="container mx-auto">
                    @foreach ($widgets_on_that_location as $widget)
                        <x-widget :widget="$widget"></x-widget>
                    @endforeach
                </div>
            </div>
        </x-slot>
    @endforeach
    <h1 class="text-3xl font-black mb-3">{{ $page->title }}</h1>
    <div class="page">{!! $page->body !!}</div>
</x-guest-layout>
