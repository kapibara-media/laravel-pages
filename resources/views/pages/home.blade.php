<x-guest-layout>
    @foreach ($widgets as $location => $widgets_on_that_location)
        <x-slot :name="__('widgets_:location', ['location'=>$location])">
            <div class="widgets {{ __('widgets-:location', ['location' => $location]) }} py-2">
                <div class="container mx-auto">
                    @foreach ($widgets_on_that_location as $widget)
                        <x-widget :widget="$widget"></x-widget>
                    @endforeach
                </div>
            </div>
        </x-slot>
    @endforeach
    <div class="page">{!! $page->body !!}</div>
</x-guest-layout>
