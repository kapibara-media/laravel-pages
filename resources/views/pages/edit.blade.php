<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight flex flex-row justify-between items-center">
            {{ __('Bewerk deze pagina') }}
            <a href="/{{ $page->slug }}" target="blank"
                class="block py-2 px-3 my-1 rounded font-bold text-gray-600 hover:text-gray-100 bg-blue-300 hover:bg-blue-600">{{ __('Bekijk deze pagina') }}</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mb-12 bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form class="" action="{{ route('page.update', $page) }}" method="POST">
                        @csrf
                        <div class="my-4">
                            <x-label for="title" :value="__('Pagina titel')" />
                            <x-input id="title" class="w-full" type="text" name="title"
                                :value="old('title', $page->title)" required autofocus
                                placeholder="Titel van de pagina" />
                        </div>
                        <div class="my-4">
                            <x-label for="slug" :value="__('Pagina URL')" />
                            <x-input id="slug" class="w-full" type="text" name="slug"
                                :value="old('slug', $page->slug)" required placeholder="URL van de pagina" />
                        </div>
                        <div class="my-4">
                            <x-label for="body" :value="__('Tekst')" />
                            <x-editor name="body" id="body" :value="old('body', $page->body)" />
                        </div>
                        <div class="my-4">
                            <x-label for="label" :value="__('Navigatie label')" />
                            <x-input id="label" class="w-full" type="text" name="label"
                                :value="old('label', $page->label)" placeholder="Navigatielabel van de pagina" />
                        </div>
                        <div class="my-4 flex justify-between">
                            <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                href="{{ route('page.index') }}">
                                {{ __('Annuleren') }}
                            </a>
                            <x-button>{{ __('Opslaan') }}</x-button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="mb-12 bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form class="" action="{{ route('page.widgets', $page) }}" method="POST">
                        @csrf
                        <div class="my-4"
                            x-data="{dragging: null, dropping: null, drag_item: null, changed: false}" id="widget-list">
                            @foreach ($widgets as $index => $widget)
                                <x-admin.widget :widget="$widget" :index="$index"
                                    :widgetLocation="$widget->pivot->location ?? ''" :locations="$locations">
                                </x-admin.widget>
                            @endforeach
                        </div>
                        <div class="my-4 flex justify-between">
                            <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                href="{{ route('page.index') }}">
                                {{ __('Annuleren') }}
                            </a>
                            <x-button>{{ __('Opslaan') }}</x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    let saveOrder = function() {
        let list = document.getElementById('widget-list').getElementsByClassName('widget-order-input');
        var index = 1;
        for (let item of list) {
            item.value = index++;
        }
    };
</script>
