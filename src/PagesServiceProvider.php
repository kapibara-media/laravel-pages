<?php

namespace Kapibara\Pages;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Kapibara\Pages\View\Components\Editor;
use Kapibara\Pages\View\Components\Widget;
use Kapibara\Pages\View\Components\Admin\Widget as AdminWidget;

class PagesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'pages');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'pages');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        Blade::component('editor', Editor::class);
        Blade::component('widget', Widget::class);
        Blade::component('admin.widget', AdminWidget::class);

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('pages.php'),
                __DIR__ . '/../config/theme.php' => config_path('theme.php'),
            ], 'config');

            // Publishing the views.
            $this->publishes([
                __DIR__ . '/../resources/views' => resource_path('views/vendor/pages'),
            ], 'views');

            // Publishing assets.
            $this->publishes([
                __DIR__ . '/../resources/assets' => public_path('vendor/pages'),
            ], 'assets');

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/pages'),
            ], 'lang');*/

            // Registering package commands.
            // $this->commands([]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'pages');

        $this->app->singleton('menu', function () {
            return new Menu;
        });
    }
}
