<?php
namespace Kapibara\Pages;

use Kapibara\Pages\Models\Page;
use Illuminate\Support\Facades\Cache;

class Menu
{
    public function render(string $classes = '')
    {
        $pages = Cache::remember('pages.published', 600, function () {
            return Page::published()->get(['id', 'slug', 'label']);
        });
        return view('pages::menu', compact('pages', 'classes'))->render();
    }
}