<?php

namespace Kapibara\Pages\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WidgetCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'          => 'required',
            'title'         => 'required',
            'body'          => '',
            'action_url'    => '',
            'action_label'  => '',
            'img_url'    => '',
            'img_alt'  => '',
            'img_caption'  => '',
            'video_id' => '',
            'video_width' => '',
            'video_height' => '',
        ];
    }
}
