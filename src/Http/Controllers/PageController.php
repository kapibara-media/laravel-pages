<?php

namespace Kapibara\Pages\Http\Controllers;

use Illuminate\Http\Request;
use Kapibara\Pages\Models\Page;
use Kapibara\Pages\Models\Widget;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Kapibara\Pages\Http\Requests\PageRequest;

class PageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $page = Page::whereSlug('home')->firstOrFail();
        $widgets = $page->widgets->groupBy('pivot.location');
        return view('pages::pages.home', compact('page', 'widgets'));
    }

    public function show(Request $request)
    {
        $slug = $request->path();
        $page = Page::with('widgets')->whereSlug($slug)->firstOrFail();
        return view('pages::pages.show', ['page' => $page, 'widgets' => $page->widgets->groupBy('location')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('pages::pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages::pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $data = $request->only(['title', 'body', 'label']);
        $data['body'] = $data['body'] ?? '';
        $data['label'] = $data['label'] ?: $data['title'];
        $page = Page::create($data);
        return redirect(route('page.edit', $page))->with('success', 'De nieuwe pagina is aangemaakt');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $widgets = Widget::all()->merge($page->widgets)->sortBy('pivot.order');
        $locations = config('theme.locations');
        return view('pages::pages.edit', compact('page', 'widgets', 'locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, Page $page)
    {
        $data = $request->only(['title', 'slug', 'body', 'label']);
        $data['body'] = $data['body'] ?? '';
        $data['label'] = $data['label'] ?: $data['title'];
        $page->update($data);
        return redirect(route('page.edit', $page))->with('success', 'De pagina is aangepast');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function widgets(Request $request, Page $page)
    {
        $validated = $request->validate([
            'widgets' => 'required|array',
        ]);
        $page->load('widgets');
        foreach ($validated['widgets'] as $id => $widget) {
            if ($widget['location'] !== null) {
                if ($page->widgets->where('id', $id)->count() === 0) {
                    $page->widgets()->attach($id, ['location' => $widget['location'], 'order' => $widget['order']]);
                } else {
                    $page->widgets()->updateExistingPivot($id, ['location' => $widget['location'], 'order' => $widget['order']]);
                }
            } else {
                $page->widgets()->detach($id);
            }
        }
        return redirect(route('page.edit', $page))->with('success', 'De widgets zijn aangepast');
    }

    /**
     * Publish the specified page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function publish(Request $request, Page $page)
    {
        $page->update(['published_at' => now(config('app.timezone'))]);
        Cache::forget('pages.published');
        return redirect(route('page.index'))->with('success', 'Deze pagina is nu gepubliceerd');
    }

    /**
     * Publish the specified page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function unpublish(Request $request, Page $page)
    {
        $page->update(['published_at' => null]);
        Cache::forget('pages.published');
        return redirect(route('page.index'))->with('warning', 'De pagina is niet meer gepubliceerd');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
