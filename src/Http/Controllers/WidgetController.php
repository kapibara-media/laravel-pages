<?php

namespace Kapibara\Pages\Http\Controllers;

use Illuminate\Http\Request;
use Kapibara\Pages\Models\Widget;
use Illuminate\Routing\Controller;
use Kapibara\Pages\Http\Requests\WidgetCreateRequest;
use Kapibara\Pages\Http\Requests\WidgetUpdateRequest;

class WidgetController extends Controller
{
    public function index()
    {
        $widgets = Widget::all();
        return view('pages::widget.index', compact('widgets'));
    }

    public function create()
    {
        return view('pages::widget.create');
    }

    public function store(WidgetCreateRequest $request)
    {
        Widget::create($request->only([
            'type', 'title', 'body', 'action_url', 'action_label', 'img_url', 'img_alt', 'img_caption', 'video_id', 'video_width', 'video_height'
        ]));
        return redirect(route('widget.index'))->with('success', 'Widget is opgeslagen');
    }

    public function edit(Widget $widget)
    {
        return view('pages::widget.edit', \compact('widget'));
    }

    public function update(WidgetUpdateRequest $request, Widget $widget)
    {
        $widget->update($request->only([
            'type', 'title', 'body', 'action_url', 'action_label', 'img_url', 'img_alt', 'img_caption', 'video_id', 'video_width', 'video_height'
        ]));
        return redirect(route('widget.index'))->with('success', 'Widget is opgeslagen');
    }
}
