<?php

namespace Kapibara\Pages\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Kapibaramedia\Pages\Skeleton\SkeletonClass
 */
class Pages extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'pages';
    }
}
