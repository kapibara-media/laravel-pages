<?php

namespace Kapibara\Pages\View\Components\Admin;

use Illuminate\View\Component;

class Widget extends Component
{
    /**
     * The alert widget.
     *
     * @var string
     */
    public $widget;

    /**
     * Create the component instance.
     *
     * @param  string  $widget
     * @param  string  $disabled
     * @return void
     */
    public function __construct($widget)
    {
        $this->widget = $widget;
    }

    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        return view("pages::components.admin.widget");
    }
}
