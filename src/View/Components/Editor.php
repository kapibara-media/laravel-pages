<?php

namespace Kapibara\Pages\View\Components;

use Illuminate\View\Component;

class Editor extends Component
{
    /**
     * The alert value.
     *
     * @var string
     */
    public $value;

    /**
     * The alert disabled.
     *
     * @var string
     */
    public $disabled;

    /**
     * Create the component instance.
     *
     * @param  string  $value
     * @param  string  $disabled
     * @return void
     */
    public function __construct($value, $disabled = false)
    {
        $this->value = $value;
        $this->disabled = $disabled;
    }

    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        return view('pages::components.editor');
    }
}
