<?php

namespace Kapibara\Pages\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Page extends Model
{
    protected $guarded = [];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($page) {
            $page->slug = \Str::slug($page->title);
        });
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published_at' => 'datetime',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Scope a query to only include published pages
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished(Builder $query): Builder
    {
        return $query->whereNotNull('published_at')->where('published_at', '<=', now(config('app.timezone')));
    }

    /**
     * Determines whether the page is currently published
     * @return boolean
     */
    public function getPublishedAttribute(): bool
    {
        return $this->published_at !== null &&
            $this->published_at <= now(config('app.timezone'));
    }

    /**
     * Publish the page, set published_at date to now
     * @return self
     */
    public function publish(): self
    {
        $this->update(['published_at' => now(config('app.timezone'))]);
        return $this;
    }

    /**
     * Take the page offline, set published_at date to null
     * @return self
     */
    public function offline(): self
    {
        $this->update(['published_at' => null]);
        return $this;
    }

    /**
     * Get all of the widgets for the post.
     */
    public function widgets(): MorphToMany
    {
        return $this->morphToMany(Widget::class, 'widgetable')->withPivot(['location', 'order'])->orderBy('order');
    }

    /**
     * The Excerpt returns part of the description for listing purposes
     * 
     * @return string
     */
    public function getExcerptAttribute(): string
    {
        return \Str::limit(\strip_tags($this->body), config('pages.excerpt-length'));
    }
}
