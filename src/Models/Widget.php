<?php

namespace Kapibara\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $guarded = [];

    public function pages()
    {
        return $this->morphedByMany(Page::class, 'widgetable');
    }
}
