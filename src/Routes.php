<?php

namespace Kapibara\Pages;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Kapibara\Pages\Http\Controllers\PageController;
use Kapibara\Pages\Http\Controllers\WidgetController;

class Routes
{
    public static function public(): void
    {
        /**
         * PUBLIC ROUTES FOR PAGES
         */
        if (Schema::hasTable('pages')) {
            \Kapibara\Pages\Models\Page::published()->each(function ($page) {
                Route::get($page->slug, [PageController::class, 'show'])->name('page.show');
            });
        }
    }

    public static function admin(): void
    {
        /**
         * ADMIN ROUTES FOR PAGES
         */
        Route::get('/pages', [PageController::class, 'index'])->name('page.index');
        Route::get('/page/write', [PageController::class, 'create'])->name('page.create');
        Route::post('/page/write', [PageController::class, 'store'])->name('page.store');
        Route::get('/page/{page}/update', [PageController::class, 'edit'])->name('page.edit');
        Route::post('/page/{page}/update', [PageController::class, 'update'])->name('page.update');
        Route::post('/page/{page}/widgets', [PageController::class, 'widgets'])->name('page.widgets');
        Route::post('/page/{page}/publish', [PageController::class, 'publish'])->name('page.publish');
        Route::post('/page/{page}/unpublish', [PageController::class, 'unpublish'])->name('page.unpublish');

        /**
         * ADMIN ROUTES FOR WIDGETS
         */
        Route::get('/widgets', [WidgetController::class, 'index'])->name('widget.index');
        Route::get('/widget/create', [WidgetController::class, 'create'])->name('widget.create');
        Route::post('/widget/create', [WidgetController::class, 'store'])->name('widget.store');
        Route::get('/widget/{widget}/update', [WidgetController::class, 'edit'])->name('widget.edit');
        Route::post('/widget/{widget}/update', [WidgetController::class, 'update'])->name('widget.update');
    }
}
