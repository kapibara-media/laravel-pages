<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYoutubeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('widgets', function (Blueprint $table) {
            $table->string('video_id')->nullable()->after('img_caption');
            $table->integer('video_width')->nullable()->after('video_id');
            $table->integer('video_height')->nullable()->after('video_width');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('widgets', function (Blueprint $table) {
            $table->dropColumn('video_id');
            $table->dropColumn('video_width');
            $table->dropColumn('video_height');
        });
    }
}
