<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWidgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('title');
            $table->text('body')->nullable();
            $table->string('action_url', 1024)->nullable();
            $table->string('action_label')->nullable();
            $table->string('img_url')->nullable();
            $table->string('img_alt')->nullable();
            $table->string('img_caption')->nullable();
            $table->timestamps();
        });

        Schema::create('widgetables', function (Blueprint $table) {
            $table->bigInteger('widget_id')->unsigned();
            $table->bigInteger('widgetable_id')->unsigned();
            $table->string('widgetable_type');
            $table->string('location')->default('center');

            $table->foreign('widget_id')->references('id')->on('widgets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widgetables');
        Schema::dropIfExists('widgets');
    }
}
